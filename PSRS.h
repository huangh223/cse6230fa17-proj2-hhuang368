#ifndef _MPI_PSRS_H_
#define _MPI_PSRS_H_

#include <stdlib.h>
#include <stdint.h>

void MPI_PSRS(
	Proj2Sorter sorter, size_t numKeysLocal, uint64_t *keys, 
	int compareKey(const void *a, const void *b)
);

#endif
#ifndef _CROSSSTEP_EXCHANGER_H_
#define _CROSSSTEP_EXCHANGER_H_

#include <mpi.h>
#include "Utils.h"

class cCrossstepExchanger
{
	public:
		cCrossstepExchanger();
		~cCrossstepExchanger();
		
		void bindCommunicator(MPI_Comm *comm);
		
		void setLocalVectorSegOffsets(const INDEX_TYPE *local_seg_offsets, const INDEX_TYPE _x_length);  

		void setFetchVectorSegRange(const INDEX_TYPE fetch_spos, const INDEX_TYPE fetch_epos);

		void exchangeVectorSeg(FLOAT_TYPE *local_x, const INDEX_TYPE local_x_offset, FLOAT_TYPE *fetch_x, const INDEX_TYPE fetch_x_offset);
		
	private:
		MPI_Comm *ce_comm;
		int ce_rank, ce_comm_size;
		INDEX_TYPE x_length;
		INDEX_TYPE *ce_local_seg_offsets;
		INDEX_TYPE *ce_seg_fetch_x_spos, *ce_seg_fetch_x_epos;
		INDEX_TYPE *ce_seg_send_x_spos,  *ce_seg_send_x_epos;
		MPI_Request *mpi_reqs;
		MPI_Status  *mpi_stas;
};

#endif
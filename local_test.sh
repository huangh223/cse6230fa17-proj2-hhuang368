#!/bin/bash

# for Deepthought
#module load openmpi-1.10-x86_64

# for my workstation
#module load intel/2017.4.056

make test_proj2

date

mpirun -np 8 ./test_proj2 100 1000000 64 12450 5

date

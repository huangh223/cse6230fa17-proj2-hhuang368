#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include <assert.h>

#include "proj2sorter.h"
#include "proj2sorter_impl.h"

#include "PSRS.h"
#include "CrossstepExchanger.h"

#include <iostream>
#include <algorithm>

void MPI_PSRS(
	Proj2Sorter sorter, size_t numKeysLocal, uint64_t *keys, 
	int compareKey(const void *a, const void *b)
)
{
	int my_rank, comm_size;
	MPI_Comm_size (MPI_COMM_WORLD, &comm_size);
	MPI_Comm_rank (MPI_COMM_WORLD, &my_rank);
	
	const MPI_Comm comm_world = MPI_COMM_WORLD;
	MPI_Comm _cw = MPI_COMM_WORLD;
	cCrossstepExchanger CE;
	CE.bindCommunicator(&_cw);
	
	size_t key_size = sizeof(uint64_t);
	
	/* ----- Stage 1, local qsort ----- */
	//qsort(keys, numKeysLocal, key_size, compareKey);
	Proj2SorterSortLocal(sorter, numKeysLocal, keys, 0);

	// select local pivots
	uint64_t *local_pivot_keys = (uint64_t *) malloc(key_size * (comm_size - 1) * 2);
	size_t local_pivot_dist = numKeysLocal / comm_size / 2;
	size_t pivot_pos = local_pivot_dist;
	for (size_t i = 0; i < 2 * (comm_size - 1); i++)
	{
		local_pivot_keys[i] = keys[pivot_pos];
		pivot_pos += local_pivot_dist;
	}
	
	/* ----- Stage 2, choose global splitter ----- */
	uint64_t *global_pivot_keys = (uint64_t *) malloc(key_size * 2 * (comm_size - 1) * comm_size);
	uint64_t *global_splitters  = (uint64_t *) malloc(key_size * (comm_size - 1));
	
	// each node has the complete copy of all nodes' pivots ((P - 1) * P pivots)
	MPI_Allgather(
		local_pivot_keys,  key_size * (comm_size - 1) * 2, MPI_CHAR, 
		global_pivot_keys, key_size * (comm_size - 1) * 2, MPI_CHAR, comm_world
	);
	
	// sort all pivots
	qsort(global_pivot_keys, 2 * (comm_size - 1) * comm_size, key_size, compareKey);
	
	// choose P - 1 global splitters
	size_t global_splitter_dist = comm_size * 2;
	size_t splitter_pos = comm_size;
	for (size_t i = 0; i < comm_size - 1; i++)
	{
		global_splitters[i] = global_pivot_keys[splitter_pos];
		splitter_pos += global_splitter_dist;
	}
	
	/* ----- Stage 3, count each MPI worker's data size ----- */
	// count each MPI worker's data in local data
	long long *send_count = (long long *) malloc(sizeof(long long) * comm_size);
	memset(send_count, 0, sizeof(long long) * comm_size);
	size_t curr_part = 0;
	uint64_t curr_splitter = global_splitters[curr_part];
	
	size_t prev_pos = 0;
	for (int i = 0; i < comm_size - 1; i++)
	{
		size_t splitter_i_pos = std::lower_bound(keys, keys + numKeysLocal, global_splitters[i]) - keys;
		send_count[i] = splitter_i_pos - prev_pos;
		prev_pos = splitter_i_pos;
	}
	send_count[comm_size - 1] = numKeysLocal - prev_pos;
	
	// all-to-all exchange each MPI worker's send-recv size
	long long *recv_count = (long long *) malloc(sizeof(long long) * comm_size);
	long long final_local_len = 0;
	MPI_Alltoall(send_count, 1, MPI_LONG_LONG_INT, recv_count, 1, MPI_LONG_LONG_INT, comm_world);
	for (int i = 0; i < comm_size; i++)
		final_local_len += recv_count[i];
	
	/* ----- Stage 4, exchange data and sort ----- */
	uint64_t *final_local_data = (uint64_t *) malloc(key_size * final_local_len);
	assert(final_local_data != NULL);
	
	MPI_Request *req  = (MPI_Request *) malloc(sizeof(MPI_Request) * comm_size);
	MPI_Status *status = (MPI_Status *) malloc(sizeof(MPI_Status) * comm_size);
	
	uint64_t *local_data_cpy_src;
	
	size_t send_pos_sum = 0;
	for (int i = 0; i < comm_size; i++)
	{
		uint64_t *send_pos = keys + send_pos_sum;
		if (i == my_rank)
		{
			local_data_cpy_src = send_pos;
		} else {
			MPI_Isend(
				send_pos, send_count[i] * key_size, MPI_CHAR,
				i, my_rank * (comm_size + 1) + i, comm_world, &req[i]
			);
		}
		send_pos_sum += send_count[i];
	}
	
	long long recv_pos_sum = 0;
	for (int i = 0; i < comm_size; i++)
	{
		uint64_t *recv_pos = final_local_data + recv_pos_sum;
		if (i == my_rank)
		{
			memcpy(recv_pos, local_data_cpy_src, key_size * recv_count[i]);
		} else {
			MPI_Irecv(
				recv_pos, recv_count[i] * key_size, MPI_CHAR,
				i, i * (comm_size + 1) + my_rank, comm_world, &req[i]
			);
		}
		recv_pos_sum += recv_count[i];
	}
	
	for (int i = 0; i < comm_size; i++)
		if (i != my_rank) MPI_Wait(&req[i], &status[i]);
	
	//qsort(final_local_data, final_local_len, key_size, compareKey);
	Proj2SorterSortLocal(sorter, final_local_len, final_local_data, 0);
	
	/* ---------- Stage 5 ---------- */
	long long *final_local_data_offsets = (long long *) malloc(sizeof(long long) * (comm_size + 1));
	memset(final_local_data_offsets, 0, sizeof(long long) * (comm_size + 1));
	for (int i = 1; i <= comm_size; i++) final_local_data_offsets[i] = final_local_len;
	
	MPI_Alltoall(MPI_IN_PLACE, 1, MPI_LONG_LONG_INT, &final_local_data_offsets[1], 1, MPI_LONG_LONG_INT, comm_world);
	
	for (int i = 2; i <= comm_size; i++) 
		final_local_data_offsets[i] += final_local_data_offsets[i - 1];
	
	long long numKeysAll = (long long) comm_size     * (long long) numKeysLocal;
	long long fetch_spos = (long long) my_rank       * (long long) numKeysLocal;
	long long fetch_epos = (long long) (my_rank + 1) * (long long) numKeysLocal;
	
	CE.setLocalVectorSegOffsets(final_local_data_offsets, numKeysAll);
	CE.setFetchVectorSegRange(fetch_spos, fetch_epos);
	CE.exchangeVectorSeg(final_local_data, final_local_data_offsets[my_rank], keys, fetch_spos);
	
	// Clean up
	free(local_pivot_keys);
	free(global_pivot_keys);
	free(global_splitters);
	free(send_count);
	free(recv_count);
	free(req);
	free(status);
	free(final_local_data_offsets);
}

#ifndef _UTILS_H_
#define _UTILS_H_

#include <mpi.h>

#define CHECK_AND_FREE(ptr) if (ptr != NULL) free(ptr)

#ifndef FLOAT_TYPE
	#define FLOAT_TYPE uint64_t
#endif

#ifndef INDEX_TYPE
	#define INDEX_TYPE  long long
	#define MPI_INDEX_T MPI_LONG_LONG_INT
#endif

#endif
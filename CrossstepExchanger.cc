#include <mpi.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

#include "CrossstepExchanger.h"
#include "Utils.h"

cCrossstepExchanger::cCrossstepExchanger()
{
	ce_comm = NULL; 
	ce_rank = -1;
	ce_comm_size = -1;
	
	ce_local_seg_offsets = NULL;
	ce_seg_fetch_x_spos  = NULL;
	ce_seg_fetch_x_epos  = NULL;
	ce_seg_send_x_spos   = NULL;
	ce_seg_send_x_epos   = NULL;
	mpi_reqs = NULL; 
	mpi_stas = NULL;
}

cCrossstepExchanger::~cCrossstepExchanger()
{
	ce_comm = NULL; 
	ce_rank = -1;
	ce_comm_size = -1;
	
	CHECK_AND_FREE(ce_local_seg_offsets);
	CHECK_AND_FREE(ce_seg_fetch_x_spos);
	CHECK_AND_FREE(ce_seg_fetch_x_epos);
	CHECK_AND_FREE(ce_seg_send_x_spos);
	CHECK_AND_FREE(ce_seg_send_x_epos);
	CHECK_AND_FREE(mpi_reqs);
	CHECK_AND_FREE(mpi_stas);
}

void cCrossstepExchanger::bindCommunicator(MPI_Comm *comm)
{
	ce_comm = comm; 
	MPI_Comm_rank(*ce_comm, &ce_rank);
	MPI_Comm_size(*ce_comm, &ce_comm_size);
	
	ce_local_seg_offsets = (INDEX_TYPE*) malloc(sizeof(INDEX_TYPE) * (ce_comm_size + 1));
	ce_seg_fetch_x_spos  = (INDEX_TYPE*) malloc(sizeof(INDEX_TYPE) * ce_comm_size);
	ce_seg_fetch_x_epos  = (INDEX_TYPE*) malloc(sizeof(INDEX_TYPE) * ce_comm_size);
	ce_seg_send_x_spos   = (INDEX_TYPE*) malloc(sizeof(INDEX_TYPE) * ce_comm_size);
	ce_seg_send_x_epos   = (INDEX_TYPE*) malloc(sizeof(INDEX_TYPE) * ce_comm_size);
	mpi_reqs = (MPI_Request*) malloc(sizeof(MPI_Request) * ce_comm_size);
	mpi_stas = (MPI_Status*)  malloc(sizeof(MPI_Status)  * ce_comm_size);
	
	assert(ce_local_seg_offsets != NULL);
	assert(ce_seg_fetch_x_spos != NULL && ce_seg_fetch_x_epos != NULL);
	assert(ce_seg_send_x_spos != NULL && ce_seg_send_x_epos != NULL);
	assert(mpi_reqs != NULL && mpi_stas != NULL);
}

void cCrossstepExchanger::setLocalVectorSegOffsets(const INDEX_TYPE *local_seg_offsets, const INDEX_TYPE _x_length)
{
	x_length = _x_length;
	memcpy(ce_local_seg_offsets, local_seg_offsets, sizeof(INDEX_TYPE) * ce_comm_size);
	ce_local_seg_offsets[ce_comm_size] = x_length;
}

void cCrossstepExchanger::setFetchVectorSegRange(const INDEX_TYPE fetch_spos, const INDEX_TYPE fetch_epos)
{
	for (int i = 0; i < ce_comm_size; i++)
	{
		if ((fetch_epos <= ce_local_seg_offsets[i]) || (fetch_spos >= ce_local_seg_offsets[i + 1])) // No overlap, no data exchange, mark it
		{
			ce_seg_fetch_x_spos[i] = -1;
			ce_seg_fetch_x_epos[i] = -1;
		} else {
			if (fetch_spos > ce_local_seg_offsets[i]) ce_seg_fetch_x_spos[i] = fetch_spos;
			else ce_seg_fetch_x_spos[i] = ce_local_seg_offsets[i];
			
			if (fetch_epos < ce_local_seg_offsets[i + 1]) ce_seg_fetch_x_epos[i] = fetch_epos;
			else ce_seg_fetch_x_epos[i] = ce_local_seg_offsets[i + 1];
		}
	}
	
	// All nodes now know the segment they need to get from a given node,
	// use Alltoall to tell target node which segment it should send to this node
	MPI_Barrier(*ce_comm);
	
	MPI_Alltoall(
		ce_seg_fetch_x_spos, 1, MPI_INDEX_T, 
		ce_seg_send_x_spos,  1, MPI_INDEX_T, *ce_comm
	);
	
	MPI_Alltoall(
		ce_seg_fetch_x_epos, 1, MPI_INDEX_T,
		ce_seg_send_x_epos,  1, MPI_INDEX_T, *ce_comm
	);
}

void cCrossstepExchanger::exchangeVectorSeg(FLOAT_TYPE *local_x, const INDEX_TYPE local_x_offset, FLOAT_TYPE *fetch_x, const INDEX_TYPE fetch_x_offset)
{
	int recv_cnt = 0;
	MPI_Request  send_req;
	MPI_Datatype MPI_SEND_TYPE;
	FLOAT_TYPE *send_to_self_ptr, *recv_from_self_ptr;
	
	if (sizeof(FLOAT_TYPE) == sizeof(float))  MPI_SEND_TYPE = MPI_FLOAT;
	if (sizeof(FLOAT_TYPE) == sizeof(double)) MPI_SEND_TYPE = MPI_DOUBLE;
	
	for (int i = 0; i < ce_comm_size; i++)
	{
		if (i == ce_rank) // Do not send to self, use memcpy instead
		{
			send_to_self_ptr = local_x + ce_seg_send_x_spos[i] - local_x_offset;
			continue;  
		}
		if (ce_seg_send_x_spos[i] != -1)
		{
			// Notice: DO NOT use INDEX_TYPE, since it may be not compatible with MPI_Irecv parameters
			int send_size = ce_seg_send_x_epos[i] - ce_seg_send_x_spos[i];
			FLOAT_TYPE *send_pos = local_x + ce_seg_send_x_spos[i] - local_x_offset;
			
			MPI_Isend(send_pos, send_size, MPI_SEND_TYPE, i, ce_rank, *ce_comm, &send_req);
		}
	}
	
	for (int i = 0; i < ce_comm_size; i++)
	{
		if (i == ce_rank) // Do not recv from self, use memcpy instead
		{
			recv_from_self_ptr = fetch_x + ce_seg_fetch_x_spos[i] - fetch_x_offset;
			continue; 
		}
		if (ce_seg_fetch_x_spos[i] != -1)
		{
			// Notice: DO NOT use INDEX_TYPE, since it may be not compatible with MPI_Irecv parameters
			int fetch_size = ce_seg_fetch_x_epos[i] - ce_seg_fetch_x_spos[i];
			FLOAT_TYPE *fetch_pos = fetch_x + ce_seg_fetch_x_spos[i] - fetch_x_offset;
			
			MPI_Irecv(fetch_pos, fetch_size, MPI_SEND_TYPE, i, i, *ce_comm, &mpi_reqs[recv_cnt]);
			recv_cnt++;
		}
	}
	
	if (send_to_self_ptr != recv_from_self_ptr)
	{
		// I guess we would have so much data to copy and can use int here...
		size_t local_seg_size = ce_seg_send_x_epos[ce_rank] - ce_seg_send_x_spos[ce_rank];
		memcpy(recv_from_self_ptr, send_to_self_ptr, sizeof(FLOAT_TYPE) * local_seg_size);
	}
	
	// Wait all Irecv to complete
	MPI_Waitall(recv_cnt, mpi_reqs, mpi_stas);
}